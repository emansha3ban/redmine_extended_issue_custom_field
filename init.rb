require "issue_format"
Redmine::Plugin.register :redmine_extended_issue_custom_field do
  name 'Redmine Extended Issue Custom Field plugin'
  author 'Ramiz Raja'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'https://bitbucket.org/ramiz_raja/redmine_extended_issue_custom_field'
  author_url 'https://www.upwork.com/freelancers/~01a019d17aaa97f235'
end
